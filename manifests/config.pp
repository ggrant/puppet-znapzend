# Configures a znapzend installation
#
# configure znapzend via zfs dataset properties
#
# @summary Configures a znapzend installation
#
# @param zfs_property_prefix
#   String to prepend to all ZFS configuration properties that are created. e.g. 'org.znapzend'.
#   Not required to be changed.
#
# @example
#   include znapzend::config
#
class znapzend::config (
  String $zfs_property_prefix,
) {

  #
  # loop over all the backups we need to manage
  #

  $::znapzend::dataset_backups.each |Hash $backup| {

    $dataset = $backup['dataset']

    $config = has_key($backup, 'config') ? {
      true => $backup['config'],
      default => {},
    }

    #
    # compose the plan string for the source dataset
    #

    $s = has_key($config, 'plan') ? {
      true => $config['plan'],
      default => $znapzend::default_plan,
    }

    #
    # validate and transform the source plan into a string
    # 
    $src_plan = $s.reduce('') |$memo, $e| {

      # remove any whitespace from the time definition string
      $keep = regsubst($e['keep'], /\s+/, '')
      $freq = regsubst($e['freq'], /\s+/, '')

      # prepend a comma seperator, unless it's the first entry
      $comma = $memo ? {
        '' => '',
        default => ','
      }

      # return value
      "${memo}${comma}${keep}=>${freq}"

    }

    # loop over destinations
    if has_key($config, 'destinations') {
      $config['destinations'].each |$dname, $dest_config| {

        # Is there a destination plan?
        $dplan = has_key($dest_config, 'dst_plan') ? {

          # yes, so validate and transform into a string
          true => $dest_config['dst_plan'].reduce('') |$memo, $e| {

            # remove any whitespace from the time definition string
            $keep = regsubst($e['keep'], /\s+/, '')
            $freq = regsubst($e['freq'], /\s+/, '')

            # prepend a comma seperator, unless it's the first entry
            $comma = $memo ? {
              '' => '',
              default => ','
            }

            # return value
            "${memo}${comma}${keep}=>${freq}"

          },

          # no - so default plan to match the source plan
          default => $src_plan,

        }

        # set concrete state for destination location
        zfs_property { "${dataset}_${dname}_dst":
          ensure   => $::znapzend::install_ensure,
          property => "${zfs_property_prefix}:dst_${dname}",
          dataset  => $dataset,
          value    => $dest_config['dst'],
        }

        # set concrete state for destination plan
        zfs_property { "${dataset}_${dname}_dst_plan":
          ensure   => $::znapzend::install_ensure,
          property => "${zfs_property_prefix}:dst_${dname}_plan",
          dataset  => $dataset,
          value    => $dplan,
        }

        # optionally set concrete state for destination pre-command
        if has_key($dest_config, 'pre_command') {
          zfs_property { "${dataset}_${dname}_precmd":
            ensure   => $::znapzend::install_ensure,
            property => "${zfs_property_prefix}:dst_${dname}_precmd",
            dataset  => $dataset,
            value    => $dest_config['pre_command'],
          }
        }

        # optionally set concrete state for destination post-command
        if has_key($dest_config, 'post_command') {
          zfs_property { "${dataset}_${dname}_pstcmd":
            ensure   => $::znapzend::install_ensure,
            property => "${zfs_property_prefix}:dst_${dname}_pstcmd",
            dataset  => $dataset,
            value    => $dest_config['post_command'],
          }
        }
      }
    }

    # set concrete state for source plan
    zfs_property { "${dataset}_src_plan":
      ensure   => $::znapzend::install_ensure,
      property => "${zfs_property_prefix}:src_plan",
      dataset  => $dataset,
      value    => $src_plan,
    }

    #
    # merge in any overrides supplied or else take defaults
    #

    $other_zfsprops = has_key($config, 'overrides') ? {
      false => $znapzend::defaults,
      default => deep_merge($znapzend::defaults,$config['overrides'])
    }

    #
    # set concrete state for other zfs properties
    #

    $other_zfsprops.each |$zfspropkey, $zfspropvalue| {
      zfs_property { "${dataset}_${zfspropkey}":
        ensure   => $::znapzend::install_ensure,
        dataset  => $dataset,
        value    => $zfspropvalue,
        property => "${zfs_property_prefix}:${zfspropkey}",
      }
    }
  }
}
