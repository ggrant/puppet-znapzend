# Manages ZnapZend service
#
# @summary Manages ZnapZend service
#
# @param enable
#   Enable service at startup
#
# @param ensure
#   whether the service should be running.
#
# @param enable
#   whether the service should be enabled at startup.
#
# @example
#   include znapzend::service
class znapzend::service (
  Variant[Enum['manual', 'mask'], Boolean] $ensure = true,
  Variant[Enum['stopped', 'running'], Boolean] $enable = true,
) {
  #
  # The service.
  #

  if $::znapzend::install_ensure == 'present' or $::znapzend::install_ensure == true {
    service { 'znapzend':
      ensure => $ensure,
      enable => $enable,
    }
  }
  else {
    service { 'znapzend':
      ensure => false,
      enable => false,
    }
  }
}

