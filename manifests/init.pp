# znapzend
#
# manage an installation of znapzend - http://www.znapzend.org
#
# znapzend source code can be found at https://github.com/oetiker/znapzend/
#
# high performance open source ZFS backup with mbuffer and ssh support
#
# znapzend creates backups of ZFS datasets, either locally or remotely, using ZFS snapshots.
#
# @summary Manage znapzend installations
#
# @param dataset_backups
#    Array of Hash of datasets to back up, with configuration
#       key: dataset
#       value: zfs dataset name (no leading slash)
#
#       key:    config
#       values: Optional Hash of configuration for the dataset.
#               If not present, Use the default plan, from 'default_plan' param for snapshots.
#
#               Contains the following elements:
#
#         key: overrides
#         value: Optional Hash which overrides the defaults when creating ZnapZend zfs configuration properties
#                These are merged with defaults, from the 'defaults' param.
#
#                Contains the following elements:
#
#                  key: enabled
#                  value: String: 'on' or 'off': switch backups on or off for a single dataset.
#
#                  key: zend_delay
#                  value: Integer: how many seconds to wait before sending snapshots
#
#                  key: mbuffer
#                  value: String: fully-qualified path to the mbuffer binary.
#
#                  key: mbuffer_size
#                  value: String: size of buffer to use with mbuffer, e.g. '1G'
#
#                  key: recursive
#                  value: String: 'on' or 'off': Whether to apply snapshot configuration to all sub-datasets (off by compiled-in default).
#
#                  key: pre_znap_cmd
#                  value: String: Command to run on source host prior to zfs dataset update activity, or 'off'.
#
#                  key: post_znap_cmd
#                  value: String: Command to run on source host post all zfs dataset update activity, or 'off'.
#
#                  key: tsformat
#                  value: String: customisable time format string naming snapshots created.
#
#         key:   plan
#         value: Optional Array specifying the the source dataset's backup plan.
#                If not specified the default plan is used.
#                Each array element is a hash containing two mandatory elements: 'keep' and 'freq'
#                Each of these contains a string specifying regex describing a length of time, e.g. '1 month', '10 days', etc.:
#                 'keep' - the length of time to keep these backups.
#                 'freq' - the frequency to take the backups.
#                see ZnapZend: https://github.com/oetiker/znapzend/blob/v0.19.1/lib/ZnapZend/Time.pm#L8 for matches needed for regex
#
#         key:   destinations
#         value: Optional Hash of hashes describing datasets/destinations to backup via ZFS send. If not specified,
#                the local plan (if any) is still executed, managing local snapshots only.
#                Contains the following elements:
#                   'dst': mandatory String describing the dataset to snapshot to, or
#                          'user@host:zpool/dataset' to specifying using SSH to send the snapshot remotely.
#                   'dst_plan': Optional: The backup plan for this destination. See 'plan' above.
#                               If not specified the source plan is used also for this destination.
#                   'pre_command': Optional: command to run on destination host prior to zfs dataset update activity
#                   'post_command': Optional: command to run on destination host post all zfs dataset update activity
#
# @param default_plan
#   Mandatory array of hash specifying the default plan, which is used as the plan if no source plan is available.
#   Used as the source (and possibly destination plan(s)) if no other plans are specified.
#   See 'plan' key in 'dataset_backups' above for details.
#   
# @param defaults
#   Mandatory Hash specifying built-in default values to use for ZnapZend zfs configuration properties.
#   Please see the 'overrides' key in 'dataset_backups' above for valid elements/types.
#
# @param install_ensure - boolean/'absent'/'present' - install/deinstall switch.
#
# @example
#   example hiera config:
#
#   module supplied defaults: see data/common.yaml
#
#   simplest hiera config, apply default backup plan to single dataset:
#
# znapzend::dataset_backups:
#   - dataset: tank
#
#
#   more complex example of deployment configuration:
#
# znapzend::dataset_backups:
#   - dataset: nmsstore1
#     config:
#       destinations:
#         remote:
#           dst: root@foo.example.com
#           dst_plan:
#             - keep: 10 years
#               freq: 1 year
#         remote2:
#           dst: root@some.other.host
#           dst_plan:
#             - keep: 1 month
#               freq: 1 day
#             - keep: 6 months
#               freq: 1 week
#   - dataset: tank2/home
#     config:
#       overrides:
#         enabled: false
#         mbuffer_size: 2G
#       destinations:
#         totank1:
#           dst: root@tank1.example.com
#           dst_plan:
#             - keep: 1 week
#               freq: 30 minutes
#

class znapzend (
  ###
  Array[Struct[{
    keep => Pattern[/^\d+\s*(seconds?|sec|s|minutes?|mins?|hours?|h|days?|d|weeks?|w|months?|mons?|m|years?|y)$/],
    freq => Pattern[/^\d+\s*(seconds?|sec|s|minutes?|mins?|hours?|h|days?|d|weeks?|w|months?|mons?|m|years?|y)$/],
  }
  ]] $default_plan,
  ###
  Struct[{
    zend_delay    => Integer,
    mbuffer_size  => String,
    recursive     => Enum['on', 'off'],
    pre_znap_cmd  => String,
    post_znap_cmd => String,
    enabled       => Enum['on', 'off'],
    mbuffer       => String,
    tsformat      => String,
  }]
  $defaults,
  ###
  Variant[Enum['absent', 'present'], Boolean] $install_ensure,
  ###
  Array[Struct[{
      dataset           => Pattern[/^[^\/]+.*/],
      config            => Optional[Struct[{
        overrides       => Optional[Struct[{
          zend_delay    => Optional[Integer],
          mbuffer_size  => Optional[String],
          recursive     => Optional[Enum['on', 'off']],
          pre_znap_cmd  => Optional[String],
          post_znap_cmd => Optional[String],
          enabled       => Optional[Enum['on', 'off']],
          mbuffer       => Optional[String],
          tsformat      => Optional[String],
        }
        ]],
        plan      => Optional[Array[Struct[{
          keep    => Pattern[/^\d+\s*(seconds?|sec|s|minutes?|mins?|hours?|h|days?|d|weeks?|w|months?|mons?|m|years?|y)$/],
          freq    => Pattern[/^\d+\s*(seconds?|sec|s|minutes?|mins?|hours?|h|days?|d|weeks?|w|months?|mons?|m|years?|y)$/],
        }
        ]]],
        destinations => Optional[Hash[String, Struct[{
          dst        => String,
          dst_plan   => Optional[Array[Struct[{
            keep     => Pattern[/^\d+\s*(seconds?|sec|s|minutes?|mins?|hours?|h|days?|d|weeks?|w|months?|mons?|m|years?|y)$/],
            freq     => Pattern[/^\d+\s*(seconds?|sec|s|minutes?|mins?|hours?|h|days?|d|weeks?|w|months?|mons?|m|years?|y)$/],
          }
          ]]],
          pre_command  => Optional[String],
          post_command => Optional[String],
        }
        ]]],
      }
      ]],
    }
  ]] $dataset_backups = [],
) {
  contain ::znapzend::install
  contain ::znapzend::config
  contain ::znapzend::service

  #
  # Class ordering for installation, configuration and service sub-classes.
  #
  if $install_ensure == 'present' or $install_ensure == true {
    Class['::znapzend::install']
    -> Class['::znapzend::config']
    ~> Class['::znapzend::service']

  }
  else {
    #
    # reverse the order if we require to uninstall.
    #
    Class['::znapzend::service']
    -> Class['::znapzend::config']
    -> Class['::znapzend::install']
  }
}
