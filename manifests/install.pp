# Install znapzend
#
# We do not handle installing ZFS on Linux (ZoL) here; we assume this is done elsewhere.
#
# @summary Installs znapzend
#
# @param package_name The name of the package to install
#
# @param package_deps Array of packages to install before installing znapzend 
#
# @example
#   include znapzend::install
class znapzend::install (
  String $package_name,
  Array $package_deps = [],
) {
  package { $package_deps:
    ensure => installed,
  }

  package { $package_name:
    ensure  => $::znapzend::install_ensure,
    require => Package[$package_deps],
  }
}
